#** Teste-Epicom **#

##*** Tecnologias ***##
* JAVA 1.8
* Spring 4
* Spring Boot
* Spring MVC
* Spring JPA
* HSQLDB
* API - Epicom
* Maven
* JUnit

##*** Resumo ***##
Este projeto foi toatalmente criado por mim(Lucas Martins Ramos) como teste técnico para a empresa Epicom.
O Projeto foi criado com Spring Boot. A classe TestEpicom contem 14 testes na qual cobre as 3 propostas para o sistema:

1. Ser capaz de receber uma notificação de criação de SKU e atualizar uma base local com os dados do SKU;
2. Fornecer uma API REST quer permita criar, remover, atualizar e listar os SKUs cadastrados localmente.
3. Fornecer um endpoint nesta mesma API que retorne apenas os SKUs disponíveis que possuam preços entre 10.00 e 40.00. A lista retornada deve estar ordenada ascendentemente pelo preço de cada SKU.

Utilizei o tratamento de excpetion do Spring(pessoalmente acho muito bom), e as exceptions são controladas através da classe(controller)  "ExceptionController".
Todo o Crud é realizado em uma base local HSQLDB(iniciada com o Spring Boot).

Procurei ultilizar os últimos frameworks do mercado em suas últimas versões. Os módulos do Spring sempre são muito bem documentados e possuem grande visibilidade no mercado OpenSource, por isso resolvi utilizar(além de ser uma das minhas opções preferidas).

O Spring MVC possuí um conversor de objeto pela anotação @ResponseBody, onde o mesmo converte a resposta de Objeto para JSON, isso facilita imensamente a integração com FrontEnd. Poderiamos integrar de forma muito rápida com o AngularJS, deixando uma interface para usuário muito mais bonita e mais performática do que Faces(PrimeFaces, RichFaces ou até mesmo JSF puro), pois tiramos a responsabilidade do BackEnd de tratar as requisições e ciclo de vida do FrontEnd(Na minha opinião Spring ou outro bom framework MVC + AngularJS são ótimas combinações nos dias atuais).

O Spring Boot, na qual meu projeto se baseia, é altamente escalável, pois é orientado a Micro Serviços. Dessa forma, caso necessário poderíamos modular nosso sistema para ser micro-serviços, tendo uma escalabilidade maior, interoperabilidade maior e balanceamento muito mais inteligente.


###*** Finalizando... ***###

Agradeço desde já pela oportunidade de realizar este teste, espero que gostem.

Obrigado,
***Lucas =)***