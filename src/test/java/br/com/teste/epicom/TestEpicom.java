package br.com.teste.epicom;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.teste.epicom.TesteEpicomApplication;
import br.com.teste.epicom.entity.Notificacao;
import br.com.teste.epicom.entity.Parametros;
import br.com.teste.epicom.entity.Sku;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.google.gson.Gson;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { TesteEpicomApplication.class })
@WebAppConfiguration
public class TestEpicom {
	private MockMvc mockMvc;
	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testRecebeuNotificacao() throws Exception {
		Notificacao vo = new Notificacao();
		vo.setTipo("criacao_sku");
		vo.setDataEnvio("2015-07-14T13:56:36");
		Parametros parametros = new Parametros();
		parametros.setIdProduto(new Long(270229));
		parametros.setIdSku(new Long(322358));
		vo.setParametros(parametros);
		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/recebeNotificacaoJson").contentType(
						MediaType.APPLICATION_JSON).content(json)).andExpect(
				status().isOk());
	}

	/**** Testes Para Consulta ***/
	@Test
	public void testConsultaSkuComId() throws Exception {
		testRecebeuNotificacao();
		Sku vo = new Sku();
		vo.setId(new Long(322358));
		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/consultar").contentType(MediaType.APPLICATION_JSON)
						.content(json)).andExpect(status().isOk());
		// .andExpect(jsonPath("$.id", is("322358")));
	}
	
	@Test
	 public void testConsultaSkuSemId() throws Exception {
	        Sku vo = new Sku();
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/consultar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }
	
	@Test
	 public void testConsultaSkuInexistente() throws Exception {
	        Sku vo = new Sku();
	        vo.setId(new Long(12));
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/consultar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }

	/**** Testes Para Incluir ***/
	@Test
	public void testIncluirSkuComCamposNull() throws Exception {
		Sku vo = new Sku();
		vo.setDisponivel(true);
		vo.setForaDeLinha(false);
		//vo.setId(new Long(123));
		//vo.setNome("Produto 1 - Crud");
		vo.setPreco(new Float(20));

		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/incluir").contentType(MediaType.APPLICATION_JSON)
						.content(json)).andExpect(
				status().isInternalServerError());
	}

	@Test
	public void testIncluirSku() throws Exception {
		Sku vo = new Sku();
		vo.setDisponivel(true);
		vo.setForaDeLinha(false);
		vo.setId(new Long(124));
		vo.setNome("Produto 2 - Crud");
		vo.setPreco(new Float(20));

		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/incluir").contentType(MediaType.APPLICATION_JSON)
						.content(json)).andExpect(
				status().isOk());
	}
	
	
	/**** Testes Para Atualizar ***/
	@Test
	 public void testAltualizarSkuSemId() throws Exception {
	        Sku vo = new Sku();
	        vo.setDisponivel(true);
	        vo.setForaDeLinha(false);
	        vo.setNome("Sku Teste 3 - Crud");
	        vo.setPreco(new Float(33));
	        
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/atualizar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }
	
	@Test
	 public void testIncluiEAltualizarSKuComId() throws Exception {
			testIncluirSku(new Long(125));
			Sku vo = new Sku();
			vo.setDisponivel(true);
			vo.setForaDeLinha(false);
			vo.setId(new Long(125));
			vo.setNome("Produto Atualizado 1 - Crud");
			vo.setPreco(new Float(10));
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/atualizar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isOk());
	 }
	
	@Test
	 public void testIncluiEAltualizarSkuComPrecoNull() throws Exception {
		testIncluirSku(new Long(126));
		Sku vo = new Sku();
		vo.setDisponivel(true);
		vo.setForaDeLinha(false);
		vo.setId(new Long(125));
		vo.setNome("Produto Atualizado 2 - Crud");
		//vo.setPreco(new Float(10));
        Gson gson = new Gson();
        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/atualizar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }
	
	public void testIncluirSku(Long id) throws Exception {
		Sku vo = new Sku();
		vo.setDisponivel(true);
		vo.setForaDeLinha(false);
		vo.setId(id);
		vo.setNome("Produto 2 - Crud");
		vo.setPreco(new Float(20));

		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/incluir").contentType(MediaType.APPLICATION_JSON)
						.content(json)).andExpect(
				status().isOk());
	}
	
	public void testIncluirSku(Long id, Float preco) throws Exception {
		Sku vo = new Sku();
		vo.setDisponivel(true);
		vo.setForaDeLinha(false);
		vo.setId(id);
		vo.setNome("Produto 2 - Crud");
		vo.setPreco(preco);

		Gson gson = new Gson();
		String json = gson.toJson(vo);

		this.mockMvc.perform(
				get("/incluir").contentType(MediaType.APPLICATION_JSON)
						.content(json)).andExpect(
				status().isOk());
	}
	
	/**** Testes Para Exclusão ***/
	
	@Test
	 public void testDeletarSkuComId() throws Exception {
			testIncluirSku(new Long(127));
	        Sku vo = new Sku();
	        vo.setId(new Long(127));
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/deletar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isOk());
	 }
	
	@Test
	 public void testDeletarSkuSemId() throws Exception {
			testIncluirSku(new Long(128));
	        Sku vo = new Sku();
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/deletar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }
	
	@Test
	 public void testDeletarSkuInexistente() throws Exception {
	        Sku vo = new Sku();
	        vo.setId(new Long(129));
	        Gson gson = new Gson();
	        String json = gson.toJson(vo);
	        
	        this.mockMvc.perform(get("/deletar")
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(json))
	            .andExpect(status().isInternalServerError());
	 }
	
	/**** Testes Para EndPoint - Retornar Skus por faixa de preco ***/
	@Test
	public void testConsultaPorPreco() throws Exception {
		testIncluirSku(new Long(130), new Float(2.6));
		testIncluirSku(new Long(131), new Float(20.45));
		testIncluirSku(new Long(132), new Float(5));
		testIncluirSku(new Long(133), new Float(50));
		testIncluirSku(new Long(134), new Float(30));
		testIncluirSku(new Long(135), new Float(25));
		testIncluirSku(new Long(136), new Float(22));
		this.mockMvc.perform(
				get("/consultarPorPreco?precoMin=10&precoMax=40")).andExpect(status().isOk());
	}
	
	@Test
	public void testConsultaPorPrecoInexistentes() throws Exception {
		testIncluirSku(new Long(137), new Float(2.6));
		testIncluirSku(new Long(138), new Float(5));
		testIncluirSku(new Long(139), new Float(50));
		this.mockMvc.perform(
				get("/consultarPorPreco?precoMin=60&precoMax=80")).andExpect(status().isInternalServerError());
	}

}
