package br.com.teste.epicom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.teste.epicom.entity.Sku;

@Repository
public interface SkuRepository extends CrudRepository<Sku, Long>{
	public Sku findById(Long id);
	
	@Modifying
	@Transactional
	@Query("delete from Sku u where u.id = :id")
	public void deleteById(@Param("id")Long id);
	
	@Query("select u from Sku u where u.preco >= :precoMin and u.preco <= :precoMax order by u.preco")
	public List<Sku> findByPrecoBetween(@Param("precoMin") Float precoMin, 
										@Param("precoMax")Float precoMax);
}
