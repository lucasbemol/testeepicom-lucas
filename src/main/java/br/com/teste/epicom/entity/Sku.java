package br.com.teste.epicom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Sku {
	
	@Id
	@GeneratedValue
	@JsonIgnore
	private Long idMyTable;
	
	@NotNull(message = "Campo Obrigatorio!")
	@Column(unique=true , nullable = false)
	private Long id;
	
	@NotNull(message = "Campo Obrigatorio!")
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = true)
	private String modelo;
	
	@Column(nullable = true)
	private String ean;
	
	@NotNull(message = "Campo Obrigatorio!")
	@Column(nullable = false)
	private boolean foraDeLinha;
	
	@NotNull(message = "Campo Obrigatorio!")
	@Column(nullable = false)
	private Float preco;
	
	@Column(nullable = true)
	private Float precoDe;
	
	@NotNull(message = "Campo Obrigatorio!")
	@Column(nullable = false)
	private boolean disponivel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public boolean isForaDeLinha() {
		return foraDeLinha;
	}

	public void setForaDeLinha(boolean foraDeLinha) {
		this.foraDeLinha = foraDeLinha;
	}

	public Float getPreco() {
		return preco;
	}

	public void setPreco(Float preco) {
		this.preco = preco;
	}

	public Float getPrecoDe() {
		return precoDe;
	}

	public void setPrecoDe(Float precoDe) {
		this.precoDe = precoDe;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
	/* Atributos nao utilizados no sistema, porem mapeados conforme JSON*/
	//private Imagens imagens;
	//private Grupos grupos;
	
	
}


