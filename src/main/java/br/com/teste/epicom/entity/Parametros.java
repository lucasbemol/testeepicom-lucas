package br.com.teste.epicom.entity;

public class Parametros {
	private Long idProduto;
	private long idSku;
	
	public Long getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}
	public long getIdSku() {
		return idSku;
	}
	public void setIdSku(long idSku) {
		this.idSku = idSku;
	}
	
	
}
