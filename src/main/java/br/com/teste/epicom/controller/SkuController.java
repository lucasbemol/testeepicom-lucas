package br.com.teste.epicom.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.teste.epicom.entity.Notificacao;
import br.com.teste.epicom.entity.Sku;
import br.com.teste.epicom.exceptions.CampoObrigatorioException;
import br.com.teste.epicom.exceptions.SkuException;
import br.com.teste.epicom.service.SkuService;

@RestController
public class SkuController {

	@Autowired
	private SkuService skuService;
	private final Logger logger = Logger.getLogger(SkuService.class);

	
	@RequestMapping(value = "/recebeNotificacaoJson")
	@ResponseBody
	public ResponseEntity<Notificacao> recebeNotificacaoJson(@RequestBody Notificacao notificacao) throws SkuException, CampoObrigatorioException, SQLIntegrityConstraintViolationException {
		logger.info("Rest recebeNotificacao");
		skuService.recebeuNotificacao(notificacao);
		return new ResponseEntity<Notificacao>(HttpStatus.OK);
	}
	
	
	@RequestMapping("/consultar")
	@ResponseBody
	public ResponseEntity<Sku> consultar(@RequestBody Sku sku) throws SkuException, CampoObrigatorioException{
		logger.info("Rest consultar sku");
		if(sku.getId() == null){
			throw new CampoObrigatorioException("Campo ID e obrigatorio para consulta!");
		}
			Sku result = skuService.consultar(sku.getId());
		return new ResponseEntity<Sku>(result, HttpStatus.OK);
	}
	
	@RequestMapping("/atualizar")
	@ResponseBody
	public ResponseEntity<Sku> atualizar(@RequestBody @Valid Sku sku, BindingResult bindingResult) throws SkuException, CampoObrigatorioException{
		logger.info("Rest atualizar Sku");
		if(bindingResult.hasErrors()){
			throw new CampoObrigatorioException(bindingResult);
		}else if(sku.getId() == null){
			throw new CampoObrigatorioException("Campo ID e obrigatorio para atualizar!");
		}
		skuService.atualizar(sku);
		return new ResponseEntity<Sku>(HttpStatus.OK);
	}
	
	@RequestMapping("/deletar")
	@ResponseBody
	public ResponseEntity<Sku> deletar(@RequestBody Sku sku) throws SkuException, CampoObrigatorioException{
		logger.info("Rest deletar Sku");
		if(sku.getId() == null){
			throw new CampoObrigatorioException("Campo ID e obrigatorio para exclusao!");
		}
		skuService.deletar(sku.getId());
		return new ResponseEntity<Sku>(HttpStatus.OK);
	}
	
	@RequestMapping("/incluir")
	@ResponseBody
	public ResponseEntity<Sku> incluir(@RequestBody @Valid Sku sku, BindingResult bindingResult) throws SkuException, CampoObrigatorioException{
		logger.info("Rest incluir sku");
		if(bindingResult.hasErrors()){
			throw new CampoObrigatorioException(bindingResult);
		}
		skuService.incluir(sku);
		return new ResponseEntity<Sku>(HttpStatus.OK);
	}
	
	@RequestMapping("/consultarPorPreco")
	public ResponseEntity<List<Sku>> consultarPorPreco(@RequestParam(value = "precoMin", defaultValue = "0") Float precoMin, @RequestParam(value = "precoMax", defaultValue = "10000") Float precoMax) throws SkuException, CampoObrigatorioException{
		logger.info("Rest consultar sku por Preco");
		if(precoMin == null || precoMax == null){
			throw new CampoObrigatorioException("Campos de precoMin e precoMax sao obrigatorios para consulta!");
		}
			List<Sku> result = skuService.consultarIntervaloDePreco(precoMin, precoMax);
		return new ResponseEntity<List<Sku>>(result, HttpStatus.OK);
	}
}