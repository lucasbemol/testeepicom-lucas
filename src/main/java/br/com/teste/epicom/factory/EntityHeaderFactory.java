package br.com.teste.epicom.factory;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
 
@Component
public class EntityHeaderFactory implements FactoryBean<HttpEntity<byte[]>>, InitializingBean {
    private HttpEntity<byte[]> entity;
 
    public HttpEntity<byte[]> getObject() {
        return entity;
    }
    public Class<HttpEntity> getObjectType() {
        return HttpEntity.class;
    }
    public boolean isSingleton() {
        return true;
    }
 
    public void afterPropertiesSet() {
    	
    	HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + new String(Base64.encodeBase64(("897F8D21A9F5A" + ":" + "Ip15q6u7X15EP22GS36XoNLrX2Jz0vqq").getBytes(Charset.forName("US-ASCII")))));
		entity = new HttpEntity<byte[]>(headers);
    }
}