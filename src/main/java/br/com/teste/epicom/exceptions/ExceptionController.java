package br.com.teste.epicom.exceptions;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.teste.epicom.service.SkuService;

@ControllerAdvice
public class ExceptionController {
	private final Logger logger = Logger.getLogger(SkuService.class);
	
	@ExceptionHandler(SkuException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public <T> ResponseEntity<T> handleCustomException(SkuException ex) {
		logger.error("SkuException: " + ex.getMessage());
		return new ResponseEntity<T> ((T)ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(CampoObrigatorioException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public <T> ResponseEntity<T> handleCustomException(CampoObrigatorioException ex) {
		logger.error("CampoObrigatorioException: " + ex.getMessage());
		if(ex.getBindingResult()!= null)
			return new ResponseEntity<T> ((T)ex.getBindingResult().getAllErrors(),HttpStatus.INTERNAL_SERVER_ERROR);
		
		return new ResponseEntity<T> ((T)ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(SQLException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public <T> ResponseEntity<T> handleCustomException(SQLIntegrityConstraintViolationException ex) {
		logger.error("Erro no DB, violacao ao efetuar operacao!: " + ex.getMessage());
		return new ResponseEntity<T> ((T)ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
