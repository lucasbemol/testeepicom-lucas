package br.com.teste.epicom.exceptions;

public class SkuException extends Exception{
	
		private static final long serialVersionUID = 1L;

		public SkuException() {
			super();
		}

		public SkuException(String pMensagem) {
			super(pMensagem);
		}

		public SkuException(Throwable pException) {
			super(pException);
		}

		public SkuException(String pMensagem, Throwable pException) {
			super(pMensagem, pException);
		}
}
