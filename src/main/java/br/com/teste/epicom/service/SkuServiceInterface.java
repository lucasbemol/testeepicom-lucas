package br.com.teste.epicom.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import br.com.teste.epicom.entity.Notificacao;
import br.com.teste.epicom.entity.Sku;
import br.com.teste.epicom.exceptions.CampoObrigatorioException;
import br.com.teste.epicom.exceptions.SkuException;

public interface SkuServiceInterface {
	
	Sku consultar(Long id) throws CampoObrigatorioException;
	void incluir(Sku sku) throws CampoObrigatorioException, SkuException, SQLIntegrityConstraintViolationException;
	void atualizar(Sku sku) throws CampoObrigatorioException, SkuException, SQLIntegrityConstraintViolationException;
	void deletar(Long id) throws CampoObrigatorioException;
	void recebeuNotificacao(Notificacao notificacao) throws CampoObrigatorioException, SkuException;
	List<Sku> consultarIntervaloDePreco(Float precoMin, Float precoMax) throws CampoObrigatorioException;
	
}
