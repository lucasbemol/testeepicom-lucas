package br.com.teste.epicom.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.teste.epicom.entity.Notificacao;
import br.com.teste.epicom.entity.Sku;
import br.com.teste.epicom.exceptions.CampoObrigatorioException;
import br.com.teste.epicom.exceptions.SkuException;
import br.com.teste.epicom.factory.EntityHeaderFactory;
import br.com.teste.epicom.repository.SkuRepository;

@Service
public class SkuService implements SkuServiceInterface {

	private static final String RELATIVE_IDENTITY_URL = "https://sandboxmhubapi.epicom.com.br/v1/";
	private final Logger logger = Logger.getLogger(SkuService.class);
	@Autowired
	SkuRepository repository;
	RestTemplate restTemplate;
	@Autowired
	EntityHeaderFactory entityHeader;

	public Sku buscaSkuEpicom(Long idProduto, Long idSku) {
		Sku result = new Sku();
		String url = RELATIVE_IDENTITY_URL
			+ "marketplace/produtos/{idProduto}/skus/{id}";
		restTemplate = new RestTemplate();
		
		Map<String, Long> params = new HashMap<String, Long>();
		params.put("idProduto", idProduto);
		params.put("id", idSku);
		
		ResponseEntity<Sku> resultado = restTemplate.exchange(url, HttpMethod.GET, entityHeader.getObject(), Sku.class, params);
		
		result = resultado.getBody();

		return result;
	}

	@Override
	public Sku consultar(Long id) throws CampoObrigatorioException {
		logger.info("Consultando Sku...");
		Sku result = repository.findById(id);
		if (result == null) {
			throw new CampoObrigatorioException("Sku Nao encontrado!");
		}
		return repository.findById(id);
	}

	@Override
	public void incluir(Sku sku) throws SkuException {
		logger.info("Incluindo sku...");
		repository.save(sku);
	}

	@Override
	public void atualizar(Sku sku) throws CampoObrigatorioException,
			SkuException {
		logger.info("Atualizando sku...");
		Sku result = repository.findById(sku.getId());
		if (result == null) {
			logger.error("Sku nao Encontrado, tentando inserir...");
			incluir(sku);
			return;
		}
		repository.save(result);
	}

	@Override
	public void deletar(Long id) throws CampoObrigatorioException {
		logger.info("Deletando Sku...");
		Sku result = repository.findById(id);
		if (result == null) {
			logger.error("Erro ao deletar Sku!");
			throw new CampoObrigatorioException(
					"Sku nao encontrado para ser excluido!");
		}
		repository.deleteById(id);
	}

	@Override
	public void recebeuNotificacao(Notificacao notificacao)
			throws CampoObrigatorioException, SkuException {
		logger.info("Atualizando dados com a notificacao recebida.");
		Sku skuEpicom = buscaSkuEpicom(notificacao.getParametros()
				.getIdProduto(), notificacao.getParametros().getIdSku());
		
		atualizar(skuEpicom);

	}
	
	@Override
	public List<Sku> consultarIntervaloDePreco(Float precoMin, Float precoMax) throws CampoObrigatorioException {
		logger.info("Consultando Sku por intervalo de preco...");
		List<Sku> result = repository.findByPrecoBetween(precoMin, precoMax);
		if (result.isEmpty()) {
			throw new CampoObrigatorioException("Sku's nao encontrado nesse intervalo de preco!");
		}
		return result;
	}

}
